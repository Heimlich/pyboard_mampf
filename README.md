# Mampf Board
Each LED letter is an own small circuit(mixed series/parallel) and can be lighted up. They are connected with screwable cable clamps. With PWM(puls width modulation) the letters can be dimmed. Power on the clamp is regulated via a transistor which gets controlled by our pyboard.
There are also three push buttons connected directly to the pyboard which can be used for controlling the board.

* Our PCB consists of two parts:
  * Pyboard
     * Programmable with Micropython(Smaller Python designed for embedded devices)
     * Files a are saved at `/flash` on the board when no sd card is prresent
     * The file `main.py` in `/flash` get executed at startup
  * Breadboard
     * Transistor controlled, screwable cable clamps
     * Power supply for the letters and also for the pyboard. The currrent by the pyboard differs from the one needed by the led letters. So there is a small circuit changing the current for the pyboard. With a small cap this circuit can be interrupted. This has to be done while powring the pyboard via Usb while developing.

### Developing
* **REMOVE the small black cap  which cuts external power supply to the pyboard while powering it with USB**
* Starting a REPL on the board can be done with `picocom /deb/ttyACM[1-n]`. Set the Pin for the arrow high
    ```
    from pyb import Pin
    Pin('X1', Pin.OUT_PP).high()
    ```
* [Rshell](https://github.com/dhylands/rshell) can be used for an simple development flow. 
   * Shell with upload function for files and Repl `sudo rshell -e vim`
   * Upload your file with `cp ~/file.py /flash/main.py` or edit with `edit /flash/main.py`
   * Start the Repl with `repl` and do a soft restart with `C-d`
   * If your code is in an other file than main.py you can start it with calling in Repl `import youfilename`

### Mapping Clamps
|             | M    | A    | M    | P    | F    | =>   |
|-------------|------|------|------|------|------|------|
|Pyboard Pin  | X2   | X4   | X3   | X7   | X8   | X1   |
|Clamp  Pos   | Pos3 | Pos6 | Pos5 | Pos7 | Pos9 | Pos1 |

### Mapping Buttons
|             | Red/Speed | Mode | Rnd |
|-------------|-----------|------|-----|
|Pyboard Pin  | Y10       | X1   | Y12 | 


### Verbose Error Output
```python
import micropython
micropython.alloc_emergency_exception_buf(100)
```