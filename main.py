# Remove Cap while programming an powering the sign
# Cap has to be left for VCC

from pyb import LED
import pyb, micropython
from pyb import Pin
from random import randint
import time
micropython.alloc_emergency_exception_buf(100)
class Mampf(object):
    def __init__(self, timer):
        self.buttons = [Pin('Y10', Pin.IN, Pin.PULL_UP),Pin('X6', Pin.IN,Pin.PULL_UP),Pin('Y12', Pin.IN, Pin.PULL_UP)]
        self.mampfs = [Pin('X2', Pin.OUT_PP), Pin('X4', Pin.OUT_PP), Pin('X3', Pin.OUT_PP), Pin('X7', Pin.OUT_PP), Pin('X8', Pin.OUT_PP), Pin('X1', Pin.OUT_PP)]
        #print(type(b[0]))
        #print(self.buttons[0].value())
        self.button_last_states = [False,False,False]
        self.speed = 500
        self.pattern = 2
        self.random = True
        timer.callback(self.cb)

    # This mechanism is not working well. Even if no button is pressed the events get triggered..
    def cb(self, tim):
        for i in range(0,1):
            cur = self.buttons[i].value()
            last = self.button_last_states[i]
            if cur == True and last == False:
                if i == 0:
                    print('Inc Speed')
                    self.increase_speed()  
                elif i == 1:
                    print('Switch Mode')
                    self.increment_pattern()
                elif i == 2:
                    print('RND')
                    self.toogle_random()
            self.button_last_states[i] = cur
    
    def increase_speed(self):
        self.speed -= 100
        if self.speed <= 0:
            self.speed = 1000

    def increment_pattern(self):
        if self.pattern <4:
            self.pattern += 1

    def toogle_random(self):
        if self.random == True:
            self.random = False
        else:
            self.random = True

    def run(self):
        while True:
            if self.random:
              self.pattern = randint(0,3)
            if self.pattern == 0:
                self.clear()
                self.lauflicht()
            elif self.pattern == 1:
                self.clear()
                self.lauflicht_stop()
            elif self.pattern == 2:
                self.clear()
                self.explosion()
            elif self.pattern == 3:
                self.clear()
                self.ffa()

    def lauflicht(self):
      print("start lauflicht")
      for pin in self.mampfs:
        pin.high()
        time.sleep_ms(self.speed)
        pin.low()
        time.sleep_ms(self.speed)

    def lauflicht_stop(self):
      print("start lauflicht_stop")
      for stop in range(0,5):
        pin = None
        for j in range(4,stop-1,-1):
          pin = self.mampfs[j]
          pin.high()
          time.sleep_ms(self.speed)
          if(j!=stop):
            pin.low()
      self.blink(self.mampfs[5],3)
      self.clear()

    def explosion(self):
      print("start explosion")
      for i in range(0,3):
          time.sleep_ms(self.speed)
          self.mampfs[2+i].high()
          self.mampfs[2-i].high()
      time.sleep_ms(self.speed)
      self.mampfs[5].high()
      time.sleep_ms(self.speed)

    def ffa(self):
      print("start ffa")
      time.sleep_ms(self.speed)
      self.blink(self.mampfs[4],3)
      time.sleep_ms(self.speed)
      self.blink(self.mampfs[4],3)
      time.sleep_ms(self.speed)
      self.blink(self.mampfs[1],3)
      time.sleep_ms(self.speed)

    def blink(self,pin,times):
      for i in range (0, times):
          pin.high()
          time.sleep_ms(self.speed)
          pin.low()
          time.sleep_ms(self.speed)

    def clear(self):
      for pin in self.mampfs:
        pin.low()
        
        
LED(2).on()
LED(4).on()
red = Mampf(pyb.Timer(4, freq=50))
red.run()